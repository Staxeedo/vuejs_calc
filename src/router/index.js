import vueRouter from "vue-router"

import App from "../App.vue"
import Calc from "../components/calc"
import Room from "../components/room"
import MainPage from "../components/mainPage"

export default new vueRouter({
  routes: [
    {
      path: "/",
      redirect: '/mainPage'
    },
    {
      path: "/calc",
      component: Calc,
      name: "calc"
    },
    {
          path: "/room",
          component: Room,
          name: "Room"
      },
      {
          path: "/mainPage",
          component: MainPage,
          name: "mainPage"
      }
  ],
  history: true
})
