// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'

import router from "./router/index.js"

Vue.use(VueRouter)

import jQuery from 'jquery'
global.jQuery=jQuery
let Bootstrap = require('bootstrap')
import 'bootstrap/dist/css/bootstrap.css'
import { Carousel } from 'bootstrap-vue/es/components'

Vue.use(Carousel);
Vue.config.productionTip = false

import VueFire from 'vuefire'
Vue.use(VueFire)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
